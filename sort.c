void quicksort(long long a[], int size){
  
  //Insertion sort has been found to be faster for arrays of size
  //14 or less. As such it is being used in place of quicksort for
  //these arrays (or parts of arrays)
  if (size >= 15){ //quicksort

    //we know this will happen at least 8 times (smallest quicksort value = 16, hence smallest amount of iterations is 8 ), so we can unfold it 8 times. 
    //This gets rid of branching instructions

    //declare these to be register variables as we will be using them often
    register long long pivot = a[size >> 1];
    register long long *left = a;
    register long long *right = a + size - 1;
    register long long temp;
    
    // First iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;        
    }
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Second iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;
    }
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Third iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;
    }        
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Fourth iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;
    }
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Fifth iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;
    }
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Sixth iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;
    }
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Seventh iteration
    if(*left < pivot){
      ++left;
    }
    else if(*right > pivot){
      --right;
    }
    else{
      temp = *left;
      *left++ = *right;
      *right-- = temp;
    }

    // Eighth+ iteration
    do{
      //Keep skipping over values that are less than the pivot and on the 
      //left hand side of the array (they are already in the correct partition)
      if(*left < pivot){
        do{
          ++left;
        }while (*left < pivot && left <= right);
      }
      //Keep skipping over values that are greater than the pivot and on the 
      //right hand side of the array (they are already in the correct partition)
      else if(*right > pivot){
        do{
          --right;
        }while (*right > pivot && left <= right);
      }
      //If the value at the left pointer is greater than the pivot and the value
      //at the right pointer is less than the pivot then we can swap these values
      //to place them in the correct partition
      else{
        temp = *left;
        *left++ = *right;
        *right-- = temp;
      }
    }while (__builtin_expect((left <= right),1));   //indicate that a branch is likely. Looking at the IA32 we discovered it expected to exit the loop usually
    //Call quicksort on the left and right partitions seperately
    quicksort(a, right - a + 1);
    quicksort(left, a + size - left);
  }
  else{ 
    //insertion sort
    register unsigned int i, hole;
    register long long temp;
    for(i = 1; __builtin_expect((i < size),1); i++){    //expecting it to usually branch (apart from the final time, this will be the case)
      temp = a[i];
      hole = i;
      //Keep shifting the current value left until we find the spot where it is
      //greater than the element before it
      while(hole > 0 && temp < a[hole-1]){
        a[hole] = a[hole-1];
        --hole;
      }
      a[hole] = temp;
    }
  }
}
